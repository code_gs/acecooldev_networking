/*******************************************************************************************
 *
 * 						Acecool Company: AcecoolDev Base for Garry's Mod
 * 				Josh 'Acecool' Moser :: CEO & Sole Proprietor of Acecool Company
 * 			 Skeleton++ Game-Mode with many useful helper-functions and algorithms
 *
 * 			 Copyright � 2001-2014 Acecool Company in Name & Josh 'Acecool' Moser
 *	as Intellectual Property Rights Owner & Sole Proprietor of Acecool Company in name.
 *
 * 					 RELEASED Under the ACL ( Acecool Company License )
 *
 *******************************************************************************************/


//
//
//
ACECOOL_ADDON_LOADED = ACECOOL_ADDON_LOADED || false;
ACECOOL_ADDON_LOADED_TIME = ACECOOL_ADDON_LOADED_TIME || 0;


 //
// Enumerations
//
local REALM_NOLOAD	= -1;
local REALM_UNKNOWN	= 0;
local REALM_CLIENT 	= 1;
local REALM_SERVER 	= 2;
local REALM_SHARED 	= 3;

local REALMS = {
	[ REALM_NOLOAD ] = "Do Not Load";
	[ REALM_UNKNOWN ] = "Unknown";
	[ REALM_CLIENT ] = "Client";
	[ REALM_SHARED ] = "Shared";
	[ REALM_SERVER ] = "Server";
};


//
// Init Auto-Loader but retain information we want to retain on auto-refresh
//
autoloader = autoloader || { };

// Allow Auto Loader Config to change during Auto-Refresh
autoloader.config = {
	// Should we use smart-refresh ( used for linux or windows servers with auto-refresh disabled so they can auto-refresh )?
	smart_refresh		= false;

	// Whether the files found deepest should be loaded first or not ( if false it'll load shallow first )
	deep_first_include	= true;

	// Should we output which files are included in exact order ( can be useful for debugging )...
	output_inclusions	= true;
};

// Reset runtime configuration ( So files can be loaded, etc... )
autoloader.runtime = autoloader.runtime || {
	base_folder = "";

	// Reset the current process on auto-refresh so that files can be loaded; won't load if it already exists...
	current_process = { };

	// Default Folder Realms ( name of folder on left = realm on right for files in folder and deeper )
	folder_realms = {
		client	= REALM_CLIENT;
		server	= REALM_SERVER;

		shared	= REALM_SHARED;
		games	= REALM_SHARED;
		maps	= REALM_SHARED;
	};

	// Default "Process Later" Folders
	process_later = {
		// maps/ folders get processed later by checking maps/<current_map>/
		maps = function( _folder, _file )
			return _folder .. _file .. "/" .. string.lower( game.GetMap( ) ) .. "/", REALM_SHARED;
		end;
		games = function( _folder, _file )
			return _folder .. _file .. "/" .. string.lower( game.GetGame( ) ) .. "/", REALM_SHARED;
		end;
	};
};


// Cache; store folder / file realms here along with file-times and other info...
autoloader.cache = autoloader.cache || {
	data = { };
};


//
// To make the autoloader work in other settings, allow us to simply set the base folder ( which is added to inclusions )
//
function autoloader:SetBaseFolder( _folder, _path )
	// Make sure the base ends with slash because it'll be prefix'd onto all includes...
	if ( !string.EndsWith( _folder, "/" ) ) then _folder = _folder .. "/"; end
	_folder = string.gsub( _folder, "//", "/" );

	self.runtime.base_folder = _folder;
	self:SetBasePath( _path );
end


//
// Returns the base folder we're working under ( typically "gamemodes/<gm_folder>/gamemode/" )
//
function autoloader:GetBaseFolder( )
	return self.runtime.base_folder || "";
end


//
// Returns the base path we're working under ( typically "LUA" )
//
function autoloader:SetBasePath( _path )
	self.runtime.base_path = _path;
end


//
// Returns the base path we're working under ( typically "LUA" )
//
function autoloader:GetBasePath( )
	return self.runtime.base_path || "LUA";
end


//
// Ensure our cache table system exists...
//
function autoloader:InitFolders( _folder, _file )
	local _cache = self.cache.data;
	if ( _folder ) then
		if ( !_cache[ _folder ] ) then
			_cache[ _folder ] = {
				folder_data = {
					realm = REALM_UNKNOWN;
				};

				files = { };
			};
		end

		if ( _file ) then
			if ( !_cache[ _folder ].files[ _file ] ) then
				_cache[ _folder ].files[ _file ] = {
					time = -1;
					realm = REALM_UNKNOWN;
				};
				self:UpdateFileTime( _folder, _file );
			end
		end
	end
end


//
// Update folder realm only if it needs it ( we aren't changing realms in real time ... or? )
//
function autoloader:SetFolderRealm( _folder, _realm )
	self:InitFolders( _folder );
	self.cache.data[ _folder ].folder_data.realm = _realm || REALM_UNKNOWN;
end


//
// Helper Function; Tells us the realm of a full-folder
//
function autoloader:GetFolderRealm( _folder )
	self:InitFolders( _folder );

	// Get realm based on folder...
	local _realm = self.cache.data[ _folder ].folder_data.realm;

	// If we have a set realm for the deepest section of the folder ( O( 1 ) ) then use it.
	if ( _realm != REALM_UNKNOWN ) then return _realm; end

	return false;
end


//
// Update folder realm only if it needs it ( we aren't changing realms in real time ... or? )
//
function autoloader:SetFolderRealm( _folder, _realm )
	self:InitFolders( _folder );
	self.cache.data[ _folder ].folder_data.realm = _realm || REALM_UNKNOWN;
end


//
// Helper Function; Returns the file realm either by using cache or calculating...
//
function autoloader:GetFileRealm( _folder, _file )
	self:InitFolders( _folder, _file );

	local _realm = REALM_UNKNOWN;
	local _files = self.cache.data[ _folder ].files;
	if ( _files ) then
		_realm = _files[ _file ] && _files[ _file ].realm || _realm;
	end

	// Then check for any overrides ( unless it is set )
	if ( _realm ) then
		return _realm
	end

	return false;
end


//
// Update folder realm only if it needs it ( we aren't changing realms in real time ... or? )
//
function autoloader:SetFileRealm( _folder, _file, _realm )
	self:InitFolders( _folder, _file );
	self.cache.data[ _folder ].files[ _file ].realm = _realm || REALM_UNKNOWN;
end


//
//
//
function autoloader:UpdateFileTime( _folder, _file )
	if ( SERVER && _folder && _file ) then
		self.cache.data[ _folder ].files[ _file ].time = file.Time( self:GetBaseFolder( ) .. _folder .. _file, self:GetBasePath( ) );
	end
end


//
//
//
function autoloader:GetFileTime( _folder, _file )
	return self.cache.data[ _folder ].files[ _file ].time, file.Time( self:GetBaseFolder( ) .. _folder .. _file, self:GetBasePath( ) );
end


//
//
//
function autoloader:BuildFolderRealm( _folder )
	// Otherwise we need to calculate it, then store it ( in each folder which doesn't have one )
	local _base = "";
	local _calculated_realm = REALM_UNKNOWN;
	local _parent_realm = REALM_UNKNOWN;
	local _realm = REALM_UNKNOWN;
	for k, v in pairs( string.Explode( "/", _folder ) ) do
		// Sorry; all folders should be lowercase...
		v = string.Trim( string.lower( v ) );

		// Make sure the folder isn't blank
		if ( v == "" ) then continue; end

		// Save the parent from getting wasted
		local _parent = _base;

		// The current folder
		_base = _base .. v .. "/";

		// Initialize the current folder table...
		-- self:InitFolders( _base );

		// Grab the parent realm, just in case we need it
		_parent_realm = self:GetFolderRealm( _parent );

		// Grab current folder realm in case it exists ( probably doesn't ), if not use parent...
		_realm = self:GetFolderRealm( _base ) || _parent_realm;

		// Then, check to see if this folder is special ( realm changer ), otherwise use realm...
		_realm = self.runtime.folder_realms[ v ] || _realm;

		// If the realm is set
		if ( _realm ) then
			// Then update the folder realm...
			self:SetFolderRealm( _base, _realm );
		end
	end

	return _realm;
end


//
// Prefix system - instead of parsing the string many times over for each 2, 3, 4 char prefix and 2, 3 char postfixes
// just generate them once and reference the values when needed.
//
function autoloader:GeneratePreAndPostFixes( _file )
	return {
		pre = {
			""; -- No need for 1
			string.sub( _file, 1, 2 ); // XXre_filename.lua
			string.sub( _file, 1, 3 ); // XXXe_filename.lua
			string.sub( _file, 1, 4 ); // XXXX_filename.lua
		};
		post = {
			""; -- No need for 1
			string.sub( _file, -6, -5 ); // _pre_filenaXX.lua
			string.sub( _file, -7, -5 ); // _pre_filenXXX.lua
		};
	};
end


//
// Checks to see if a file has pre/postfix
//
function autoloader:IsPrefix( _file, _chars, _string, _data )
	return ( _chars > 0 ) && _data.pre[ _chars ] == _string || _data.post[ math.abs( _chars ) ] == _string;
end


//
//
//
function autoloader:BuildFileRealm( _folder, _file )

	local _data = self:GeneratePreAndPostFixes( _file );

	// x files skip loading, non lua files skip loading.
	if ( self:IsPrefix( _file, 2, "x_", _data ) || self:IsPrefix( _file, 3, "_x_", _data ) || self:IsPrefix( _file, -2, "_x", _data ) || string.GetExtensionFromFilename( _file ) != "lua" ) then
		return REALM_NOLOAD;
	end

	// Check for cl_, _cl_, _cl.lua, sh_, _sh_, _sh.lua, sv_, _sv_, _sv.lua for specific loading realm...
	local _bFileStartsCL = ( self:IsPrefix( _file, 3, "cl_", _data ) || self:IsPrefix( _file, 4, "_cl_", _data ) || self:IsPrefix( _file, -3, "_cl", _data ) );
	local _bFileStartsSH = ( self:IsPrefix( _file, 3, "sh_", _data ) || self:IsPrefix( _file, 4, "_sh_", _data ) || self:IsPrefix( _file, -3, "_sh", _data ) );
	local _bFileStartsSV = ( self:IsPrefix( _file, 3, "sv_", _data ) || self:IsPrefix( _file, 4, "_sv_", _data ) || self:IsPrefix( _file, -3, "_sv", _data ) );

	// Update the realm for the include
	local _realm = self:GetFolderRealm( _folder );
	_realm = ( _bFileStartsCL ) && REALM_CLIENT || _realm;
	_realm = ( _bFileStartsSH ) && REALM_SHARED || _realm;
	_realm = ( _bFileStartsSV ) && REALM_SERVER || _realm;

	return _realm;
end


//
// Helper function to set up ProcessLater points...
//
function autoloader:ProcessLater( _folder, _realm, _callback )
	local function HandleProcessLater( )
		self:AddFolder( _folder, _realm, _callback );
	end

	hook.Add( "InitPostEntity", "ProcessLater:InitPostEntity:" .. _folder, HandleProcessLater );
	hook.Add( "OnReloaded", "ProcessLater:OnReloaded:" .. _folder, HandleProcessLater );
end


//
// Adds a folder to process...
//
function autoloader:AddFolder( _folder, _realm, _callback )
	// If the user defined a realm for this folder set, otherwise build...
	local _folder_realm = self:GetFolderRealm( _folder );
	if ( !_folder_realm && _realm ) then
		self:SetFolderRealm( _folder, _realm );
	elseif ( !_folder_realm ) then
		self:BuildFolderRealm( _folder );
	end

	// Grab all files and folders for the current folder we're in...
	local _files, _folders = file.Find( self:GetBaseFolder( ) .. _folder .. "*", self:GetBasePath( ) );

	// Process folders
	if ( istable( _folders ) ) then
		for k, v in pairs( _folders ) do
			local _file = string.Trim( string.lower( v ) );

			// Check to see if the current folder is a "load later" folder.
			local _process_later_func = self.runtime.process_later[ _file ];
			if ( _process_later_func ) then
				// If it is, grab the generated folder-name from the function we call and the default realm...
				local _folder, _realm = _process_later_func( _folder, _file );

				// Call a helper-function which creates the hooks...
				self:ProcessLater( _folder, _realm, _callback );
			else
				// Add each folder so we can process each folder, leave realm nil for it to be calculated...
				self:AddFolder( _folder .. _file .. "/", nil, _callback );
			end
		end
	end

	// Process files to be categorized / loaded, etc...
	if ( istable( _files ) ) then
		for k, v in pairs( _files ) do
			local _file = string.Trim( string.lower( v ) );
			local _ext = string.GetExtensionFromFilename( _file );
			if ( _ext != "lua" ) then continue; end

			self:IncludeFile( _folder, _file );
		end
	end
end


//
// Includes a file if
//
function autoloader:require( _file, _realm )
	local _folder = string.GetPathFromFilename( _file ) || "";
	local _new_file = string.GetFileFromFilename( _file ) || "";
	_file = ( _new_file == "" ) && _file || _new_file;
	self:IncludeFile( _folder, _file, _realm );
end


//
// Alias
//
function require_once( _file, _realm )
	autoloader:require( _file, _realm );
end


//
//
//
function autoloader:IncludeFile( _folder, _file, _override_realm )
	// Prevent loading the same file twice
	if ( autoloader.runtime.current_process[ self:GetBaseFolder( ) .. _folder .. _file ] ) then return false; end
	autoloader.runtime.current_process[ self:GetBaseFolder( ) .. _folder .. _file ] = true;

	// Initialize folder if needed.
	self:InitFolders( _folder, _file );

	// Grab cached folder realm, calculate if need be
	local _folder_realm = self:GetFolderRealm( _folder );
	if ( !_folder_realm || _folder_realm == REALM_UNKNOWN ) then
		_folder_realm = self:BuildFolderRealm( _folder );
	end

	// Grab cached file realm or calculate if need be
	local _file_realm = self:GetFileRealm( _folder, _file );
	if ( !_file_realm || _file_realm == REALM_UNKNOWN ) then
		_file_realm = self:BuildFileRealm( _folder, _file );
	end

	// Use the most logical realm ( file first otherwise folder, allow allow override for require calls )
	local _realm = ( _file_realm != REALM_UNKNOWN ) && _file_realm || _folder_realm;
	_realm = _override_realm || _realm;

	// If the realm ends up being unknown, don't do anything with the file ( happens on txt files, etc. we don't want them anyway )
	if ( _realm == REALM_UNKNOWN || _realm == REALM_NOLOAD ) then return false; end

	// We want to AddCSLuaFile / include even during smart-refresh to hopefully update the string-tables ( so new
	// users connecting get latest file without needing to manually send them )

	// AddCSLuaFile if this is the server and the file needs to be downloaded by the client
	if ( SERVER && ( _realm == REALM_CLIENT || _realm == REALM_SHARED ) ) then
		AddCSLuaFile( self:GetBaseFolder( ) .. _folder .. _file );
	end

	// Include the file if it is shared and meant for either client or server, or it is either client or server file and in respective realm
	if ( ( SERVER && _realm == REALM_SERVER ) || ( CLIENT && _realm == REALM_CLIENT ) || ( ( CLIENT || SERVER ) && _realm == REALM_SHARED ) ) then
		include( self:GetBaseFolder( ) .. _folder .. _file );
	end

	if ( self.config.output_inclusions ) then
		print( REALMS[ _realm ] .. ": " .. self:GetBaseFolder( ) .. _folder .. _file );
	end
end


//
//
//
function autoloader:LoadAddon( )
	// Reset the "current process" which contains a cache of loaded files ( to prevent loading the same one twice )
	autoloader.runtime.current_process = { };

	// Set up Load-Order. Best is SHARED, Client/Server, Addons, Content, etc...

	// We can set up base path PER AddFolder if we want to make require_once in any of the files the same dir
	// Or we can set the base once which is common between all, then use sub-folders...
	-- self:SetBasePath( "LUA" );
	self:SetBaseFolder( "acecooldev_networking/", "LUA" ); -- Second arg is optional, lets you set lsv path with path..

	self:AddFolder( "definitions/", REALM_SHARED ); -- Adding this for Linux users ( _ comes after a-z )
	self:AddFolder( "core/", REALM_SHARED ); -- Adding this for Linux users ( _ comes after a-z )
	self:AddFolder( "core_meta/", REALM_SHARED ); -- Adding this for Linux users ( _ comes after a-z )
	self:AddFolder( "shared/", REALM_SHARED );
	self:AddFolder( "client/", REALM_CLIENT );
	self:AddFolder( "server/", REALM_SERVER );
	self:AddFolder( "addons/", REALM_SHARED );

	// Example of setting the base folder and adding the current folder ( This allows any require_once( ) function
	// calls, or other calls be set to the current dir ( working dir ); this just shows an example of that...
	-- self:SetBaseFolder( "acecooldev_networking/definitions/" );
	-- self:AddFolder( "/", REALM_SHARED ); -- Adding this for Linux users ( _ comes after a-z )

	-- self:SetBaseFolder( "acecooldev_networking/core/" );
	-- self:AddFolder( "/", REALM_SHARED ); -- Adding this for Linux users ( _ comes after a-z )

	-- self:SetBaseFolder( "acecooldev_networking/core_meta/" );
	-- self:AddFolder( "/", REALM_SHARED ); -- Adding this for Linux users ( _ comes after a-z )

	-- self:SetBaseFolder( "acecooldev_networking/shared/" );
	-- self:AddFolder( "/", REALM_SHARED );

	-- self:SetBaseFolder( "acecooldev_networking/client/" );
	-- self:AddFolder( "/", REALM_CLIENT );

	-- self:SetBaseFolder( "acecooldev_networking/server/" );
	-- self:AddFolder( "/", REALM_SERVER );

	-- self:SetBaseFolder( "acecooldev_networking/addons/" );
	-- self:AddFolder( "/", REALM_SHARED );

	// Show loaded file-count if enabled or first time game loads
	if ( !ACECOOL_ADDON_LOADED ) then
		MsgC( color_white, "AcecoolDev Addon Loaded!\n" );
	else
		MsgC( color_white, "AcecoolDev Addon Refreshed!\n" );
	end

	// Some tracking data...
	ACECOOL_ADDON_LOADED = true;
	ACECOOL_ADDON_LOADED_TIME = os.time( );
end


//
// Call the Auto-Loader; this is here for initial load and auto-refresh; for disabled auto-refresh / Linux
// users, see loadaddon console command...
//
-- hook.Add( "Initialize", "CrossingFingersThatThisWorks", function( )
	autoloader:LoadAddon( );
-- end );


//
// Fake Auto-Refresh to get by the annoyance of saving multiple files for new files to show up...
//
if ( SERVER ) then
	//
	// Server=side Forced Auto-Refresh console command: loadaddon
	//
	concommand.Add( "loadaddon", function( _p, _cmd, _args )
		// Make sure Player is NULL or IsSuperAdmin, otherwise STOP. NULL player is RCON user...
		if ( _p != NULL && !_p:IsSuperAdmin( ) ) then return; end

		// Server-Side refresh
		GM = GAMEMODE;
		autoloader:LoadAddon( );
		hook.Call( "OnReloaded", GAMEMODE );

		// Client-Side refresh
		BroadcastLua( "GM = GAMEMODE; autoloader:LoadAddon( ); hook.Call( \"OnReloaded\", GAMEMODE );" );
	end );
end