//
// Data Storage System - Josh 'Acecool' Moser
// This ties heavily into the networking system. This handles all of the data...
//
if ( !data ) then
	data = { };
	data.__index = data;

	// Entity and World Data
	data.__flags = {
		public = { };
		private = { };
		links = { };
	};

	// Managed Lists
	data.__managed_lists = {
		connected_players = { };
		spawned_vehicles = { };
	};
end


//
// Important includes...
//
require_once( "shared/sh_networking.lua" );


//
// ENUMeration / CONSTs / etc...
//


// Flags and what they're meant to represent ( categories within the flag system )
FLAG_ACCOUNT	= "account";
FLAG_ADMIN		= "admin";
FLAG_BANK		= "bank";
FLAG_BANKING	= "bank";
FLAG_CHARACTER	= "character";
FLAG_CURRENCY	= "currency";
FLAG_MONEY		= "currency";
FLAG_PLAYER		= "player";
FLAG_VEHICLE	= "vehicle";
FLAG_WEAPON		= "weapon";

// Meant for temporary data, such as logging weapon color when enabling invisibility and using
// physgun so it switches to black, and then back to normal color when non-invisible...
// Or setting stamina so when you log out of admin the stamina is what it was, or ragdoll data...
// This isn't enforced and the temp category isn't purged, it's just another spot...
FLAG_TEMP		= "temp";



//
// Configuration...
//
data.__config = {
	// Delay that un-linked data be re-requested by the client; may move this to the data class
	// How long should we keep values cached before re-requesting them ( on private flags the client isn't linked to and inquires about )
	data_refresh_rate				= 3;
};


//
// Helper for data:BuildAccessors/data:AccessorFunc to process forced values; logic was the same so instead of repeating...
//
local function BuildForcedXTable( _values, _func )
	local _tab = { };
	if ( _values ) then
		if ( istable( _values ) ) then
			for k, v in pairs( _values ) do
				_tab[ ( _func && _func( v ) || v ) ] = true;
			end
		else
			_tab[ ( _func && _func( _values ) || _values ) ] = true;
		end
	end

	return _tab;
end


//
// Simple helper.. Checks to see if we're forcing values or types, and if so checks to see if value is allowed...
//
local function ForcedXChecker( _forced, _allowed, _value )
	if ( _forced ) then
		if ( !_allowed[ _value ] ) then
			return false;
		end
	end

	return true;
end


//
// Adds Getters / Setters / Adders for flag...
//
// The _flag will be set to lowercase and used as the flag, so use UpperCamel or your naming technique
// for the function naming. If you want the flag to have a different name perhaps with underscores,
// simply set name = "Invisible" in the options table, then that name overwrites the function names
// and flag stays as flag ( still set to lower )...
//
// Examples:
//
// data:BuildAccessors( _flag, _default, _private, _category, _table, _force_types, _force_values, _options )
// data:BuildAccessors( "Invisible", false, false, FLAG_PLAYER, META_PLAYER, TYPE_BOOL, nil, { get_prefix = ""; } )
// 	Creates: _p:Invisible( ); _p:SetInvisible( true/false ); _p:CanSetInvisible( _value ); where CanSet
// simple checks to see if the input value is allowed. It is automatically added inside of the Setter...
// The default value IS NOT CHECKED before creating the functions...
//
// data:BuildAccessors( "player_admin", false, false, FLAG_PLAYER, META_PLAYER, TYPE_BOOL, nil, { name = "Admin"; get_prefix = "Is"; } )
// Creates: _p:IsAdmin( ); _p:SetAdmin( true/false ); _p:CanSetAdmin( _value );
//
// You can force the type of data and you can also force the allowed values. This doesn't apply to tables
// because if you're allowing a table of data, there may be a good reason, just use the parent data-type,
// table, to declare it; I may add support but it'd mean looping through and this system is meant to be fast...
//
// NOTE: When forcing data on NON-SIMPLE-DATA-TYPES I was forced to tostring the data in order to compare
// because Vector( 0, 0, 0 ) is not the same as Vector( 0, 0, 0 ) ( 2 different references and I haven't
// created the __eq comparitor for all meta-table objects yet ). So, they'll be turned to simple data-type
// string for comparison. So for some you'll have no issue, others you may...
//
// Examples of forcing values...:									for my vehicle system	( 0 = off, 1 = left, 2 = right, 3 = hazards )
// data:BuildAccessors( "signals", false, false, FLAG_VEHICLE, META_VEHICLE, TYPE_NUMBER, { 0, 1, 2, 3 }, { name = "TurnSignals"; get_prefix = ""; } );
// Creates: _v:TurnSignals( ); _v:SetTurnSignals( 0-3 ); _v:CanSetTurnSignals( x ); Which operate "signals" flag in category "vehicle"
//
function data:BuildAccessors( _flag, _default, _private, _category, _table, _force_types, _force_values, _options )
	// Options: name will change the name used for CanSet*, Set*, Get*
	// get_prefix changes "Get" to anything in the string, for example "Is", "", "In", etc works..
	local _options = _options || {
		// Name will over-ride the _flag as the function Name...
		name		= nil;

		// Overrides the Getter Prefix from Get to anything defined such as "Is", "In", "", etc...
		get_prefix	= nil;

		// true If this function is a toggle function ( must be BOOL type forced or true/false value forced )
		toggle		= nil;

		// Adder; If this is an adder function then there must be a min and max limit to clamp to and
		// it must add to current value...
		adder		= nil;

		// If the adder gets clamped...
		adder_clamp	= nil;

		// Min and max are optional if we have an adder ( currency, etc ) but is useful for armor, etc...
		min			= nil;
		max			= nil;
	};

	// Allowed Types and Allowed Values; will be at minimum empty table or direct-access one...
	local _allowed_types = BuildForcedXTable( _force_types, nil );
	local _allowed_values = BuildForcedXTable( _force_values, tostring );

	// Function names. Easier to pre-set because I need to use: _table[ _name ] = function( ) ... end and _table[ _name ]( self, ... );
	local _funcname = ( _options.name || _flag );
	local _getprefix = ( _options.get_prefix || "Get" );

	// Controller Names
	local _canset	= "CanSet" .. _funcname;

	// Setter Names - these can alter the value
	local _setter	= "Set" .. _funcname;
	local _setmax	= "SetMax" .. _funcname;
	local _setmin	= "SetMin" .. _funcname;
	local _adder	= "Add" .. _funcname;
	local _toggle	= "Toggle" .. _funcname;

	// Getter Names - these can only give us data
	local _has		= "Has" .. _funcname;
	local _getter	= _getprefix .. _funcname;
	local _getmax	= _getprefix .. "Max" .. _funcname;
	local _getmin	= _getprefix .. "Min" .. _funcname;

	// Other
	local _clamper	= _options.adder_clamp && math.Clamp || function ( _value, _min, _max ) return _value; end


	//
	// CanSet* - Internal function used to determine whether or not the value meets the criteria to be used
	//
	debug.print( "Data:BuildAccessors", "Adding " .. _canset );
	_table[ _canset ] = function ( self, _value )
		return ForcedXChecker( _force_types, _allowed_types, TypeID( _value ) ) && ForcedXChecker( _force_values, _allowed_values, tostring( _value ) );
	end


	//
	// Set* - Sets the value
	//
	debug.print( "Data:BuildAccessors", "Adding " .. _setter );
	_table[ _setter ] = function ( self, _value )
		if ( _table[ _canset ]( self, _value ) ) then
			// If we're in adder and the value is too low or too high, set them to the limit.
			local _bTooLow = ( _options.min && _value < _table[ _getmin ]( self ) );
			local _bTooHigh = ( _options.max && _value > _table[ _getmax ]( self ) )
			if ( _options.adder && ( _bTooLow || _bTooHigh ) ) then
				if ( _bTooLow ) then
					self:SetFlag( string.lower( _flag ), _table[ _getmin ]( self ), _private, _category );
				elseif ( _bTooHigh ) then
					self:SetFlag( string.lower( _flag ), _table[ _getmax ]( self ), _private, _category );
				else
					// This should never happen, this shouldn't be here...
					error( "Error with option adder min and max..." );
				end
			else
				// Otherwise we're NOT in adder mode
				// OR we aren't exceeding max or falling below minimum so it's safe to set...
				self:SetFlag( string.lower( _flag ), _value, _private, _category );
			end

			return true;
		end

		return false;
	end


	//
	// "Get"* - Returns the value stored in this flag...
	//
	debug.print( "Data:BuildAccessors", "Adding " .. _getter );
	_table[ _getter ] = function ( self )
		return self:GetFlag( string.lower( _flag ), _default, _private, _category );
	end


	//
	// Toggle* - Only used on boolean functions
	//
	if ( _options.toggle && ( _force_types == TYPE_BOOL || _allowed_types[ TYPE_BOOL ] || ( _allowed_values[ true ] && _allowed_values[ false ] ) ) ) then
		debug.print( "Data:BuildAccessors", "Adding " .. _toggle );
		_table[ _toggle ] = function ( self )
			_setter( self, !_table[ _getter ]( self ) );
		end
	end


	// Adder Functions
	if ( _options.adder && ( _force_types == TYPE_NUMBER || _allowed_types[ TYPE_NUMBER ] ) ) then
		// If we have max set allow max functions
		if ( _options.max ) then
			//
			// SetMax* - Updates the maximum value allowed
			//
			debug.print( "Data:BuildAccessors", "Adding " .. _setmax );
			_table[ _setmax ] = function ( self, _value )
				self:SetFlag( string.lower( _flag ) .. "_max", _value, _private, _category );
			end


			//
			// "Get"Max* Which returns the maximum value allowed...
			//
			debug.print( "Data:BuildAccessors", "Adding " .. _getmax );
			_table[ _getmax ] = function ( self )
				return self:GetFlag( string.lower( _flag ) .. "_max", _options.max, _private, _category );
			end
		end

		// If we have min set allow min functions
		if ( _options.min ) then
			//
			// SetMin* - Updates the minimum value allowed
			//
			debug.print( "Data:BuildAccessors", "Adding " .. _setmin );
			_table[ _setmin ] = function ( self, _value )
				self:SetFlag( string.lower( _flag ) .. "_min", _value, _private, _category );
			end


			//
			// "Get"Min* Which returns the minimum value allowed...
			//
			debug.print( "Data:BuildAccessors", "Adding " .. _getmin );
			_table[ _getmin ] = function ( self )
				return self:GetFlag( string.lower( _flag ) .. "_min", _options.min, _private, _category );
			end
		end


		//
		// Add* which adds a value to another ( The Setter controls clamping if necessary )...
		//
		debug.print( "Data:BuildAccessors", "Adding " .. _adder );
		_table[ _adder ] = function ( self, _value )
			_table[ _setter ]( self, _table[ _getter ]( self ) + _value );
		end
	end


	//
	// Has* - Returns true if we don't input value and min exists and there is more than min
	//			Returns true if value is input and we have more than it
	//			Returns true if value is non-false/nil
	//
	debug.print( "Data:BuildAccessors", "Adding " .. _has );
	_table[ _has ] = function ( self, _value )
		if ( !_value && _options.min ) then
			return _table[ _getter ]( self ) > _table[ _getmin ]( self );
		elseif ( _value ) then
			return _table[ _getter ]( self ) >= _value;
		else
			return _table[ _getter ]( self ) && true || false;
		end
	end
end


//
// For those that want to use AccessorFunc naming technique...
//
// AccessorFunc( table tab, any key, string name, number iForce )
// AccessorFunc( META_PLAYER, "invisible", "Invisible", TYPE_BOOL )
//
// I decided to not use the AccessorFunc method because it didn't allow for all the options
// and didn't go with the normal format of the flag system( flag, default/value, private, category )
// however, data:AccessorFunc is setup in a similar method. It calls the function below with reorganized
// naming...
//
// These are identical:
// data:BuildAccessors( "player_admin", false, false, FLAG_PLAYER, META_PLAYER, TYPE_BOOL, nil, { name = "Admin"; get_prefix = "Is"; } )
// data:AccessorFunc( META_PLAYER, "player_admin", "Admin", TYPE_BOOL, nil, false, false, FLAG_PLAYER, { get_prefix = "Is"; } )
// Creates: _p:IsAdmin( ); _p:SetAdmin( true/false ); _p:CanSetAdmin( x );
//
function data:AccessorFunc( _table, _flag, _name, _force_types, _force_values, _default, _private, _category, _options )
	local _options = _options || { };
	_options.name = _name;

	data:BuildAccessors( _flag, _default, _private, _category, _table, _force_types, _force_values, _options )
end


//
// Grab the private or public data list
//
function data:GetFlags( _private )
	if ( _private ) then
		return self.__flags.private;
	end

	return self.__flags.public;
end


//
// Returns the list of players an entity is linked to
//
function data:GetLinks( _ent )
	local _id = self:GetEntityID( _ent );
	local _links = self.__flags.links[ _id ] || { };

	return _links;
end


//
// Updates the public data flags table with an inbound table ( for syncing )
//
function data:SetFlags( _flags )
	debug.print( "Data:InitializePublic", "SetFlags:", _flags );
	self.__flags.public = _flags;
end


//
// Updates the default private-list data flags table with an inbound table ( for syncing specific entities / pdata )
//
function data:SetPData( _id, _tab )
	if ( !self.__flags.private[ _id ] ) then
		self.__flags.private[ _id ] = {
			default = { };
			lists = { };
		};
	end

	debug.print( "Data:InitializePrivate", "SetPData EntityID, Data, ID:", _id, "Table:", _tab );
	self.__flags.private[ _id ] = _tab;
end


//
// Returns the entity id
//
function data:GetEntityID( _ent )
	if ( isstring( _ent ) || isnumber( _ent ) ) then
		return tonumber( _ent ) || -1;
	else
		if ( IsValid( _ent ) ) then
			return tonumber( _ent:GetID( ) ) || -1;
		else
			debug.print( "Data:GetEntityIDError", type( _ent ) .. " " .. _ent .. " ); isn't valid..." );

			return 0;
		end
	end
end


//
// Returns the entity flag value ( This is called by META_ENTITY:GetFlag( _flag, _default, _private ) )
//
function data:GetFlag( _ent, _flag, _default, _private, _category, _request )
	local _id 		= self:GetEntityID( _ent );
	local _flags 	= self:GetFlags( _private );

	if ( _id >= 0 ) then
		// If CLIENT is running this code,
		// and the variable is a private var ( which won't sync for non-LocalPlayer if different entity unless linked )
		// And making sure _ent isn't LocalPlayer ( because LP data will sync automatically; non-lp entities won't unless linked )
		if ( CLIENT && _private && _ent != LocalPlayer( ) ) then
			local _bLinked = self:IsLinked( LocalPlayer( ), _ent );

			// If the client isn't linked to this particular entity ( but the client is requesting the data ),
			// send a request, but only if the client wants it ( By default it isn't on to save networking speed,
			// and because it is more rare to request a variable than it is to want to set private data from the
			// client onto other players / objects without it being erased by the request ); this way we don't need
			// to store ALL information we know of the world / ents on the world in public realm or on other ents
			// in public realm to prevent requests... We can store our own private data on other ents such as name
			// if they tell us, etc...
			if ( !_bLinked && _request ) then
				// Get the current value of the flag to send for comparison
				local _data = _ent:GetFlag( _flag, _default, _private, _category, false );

				// Requests data from the server based on an interval if we don't have a linked data connection.
				networking:RequestFlag( ( isnumber( _request ) && _request || data.__config.data_refresh_rate ), _ent, _flag, _data, _private, _category, _default );

				debug.print( "Data:RequestPrivate", "GetFlag > RequestFlag, Entity:", _ent, "Flag:", _flag, "Default:", _default, "Private:", _private );
			end
		end

		// If our database is setup, and the GetID exists, and either the flag exists / is set, or it is false ( because values can switch between true/false and they still need to be updated )
		if ( _flags && _flags[ _id ] ) then
			local _localflags = _flags[ _id ];
			if ( _category || _category == false ) then
				if ( !_localflags.lists ) then _localflags.lists = { }; end
				if ( !_localflags.lists[ _category ] ) then _localflags.lists[ _category ] = { }; end

				_localflags = _localflags.lists[ _category ];
			else
				if ( !_localflags.default ) then _localflags.default = { }; end
				_localflags = _localflags.default;
			end
			if ( _localflags[ _flag ] || _localflags[ _flag ] == false ) then
				debug.print( "Data:Get", "GetFlag > Entity:", _ent, "Flag:", _flag, "Default:", _default, "Private:", _private, "Category:", _category );
				return _localflags[ _flag ];
			end
		end
	end

	// Return default if all else fails...
	debug.print( "Data:GetDefault", "GetFlag > Default, Entity:", _ent, "Flag:", _flag, "Value:", _value, "Private:", _private );
	return _default;
end


//
// Sets the entity flag value ( This is called by META_ENTITY:SetFlag( _flag, _value, _private ) )
//
function data:SetFlag( _ent, _flag, _value, _private, _category )
	local _id = self:GetEntityID( _ent );

	local _flags = self:GetFlags( _private );
	if ( _id >= 0 ) then
		if ( !_flags[ _id ] ) then
			_flags[ _id ] = { };
		end

		// Added to allow private-flag lists ( for addons to avoid collisions )
		// Now includes public flags by switching to an optional category system
		local _localflags = _flags[ _id ];
		if ( _category || _category == false ) then
			if ( !_localflags.lists ) then _localflags.lists = { }; end
			if ( !_localflags.lists[ _category ] ) then _localflags.lists[ _category ] = { }; end

			_localflags = _localflags.lists[ _category ];
		else
			if ( !_localflags.default ) then _localflags.default = { }; end

			_localflags = _localflags.default;
		end

		// If the data hasn't changed, don't do anything. Tables are always updated until I change that behavior
		if ( _localflags[ _flag ] == _value && !istable( _localflags[ _flag ] ) ) then
			debug.print( "Data:SetFlagNoChangeError", "No Update, Entity:", _ent, "Flag:", _flag, "Value:", _value, "Private:", _private );
			return;
		end

		// Update the value...
		debug.print( "Data:SetFlag", "Updated Flag, Entity:", _ent, "Flag:", _flag, "Value:", _value, "Private:", _private );
		_localflags[ _flag ] = _value;

		// Server code
		if ( SERVER ) then
			// If this is a private flag ( Only known to server, or server and one client )
			if ( _private ) then
				// If it's to be known with a client, let the client know, otherwise it's server only
				if ( _ent:IsPlayer( ) ) then
					debug.print( "Data:SetPrivateFlag", "Updated Flag, Entity:", _ent, "Flag:", _flag, "Value:", _value, "Private:", _private );
					networking:SendToClient( "EntitySetFlag", _ent, _id, _flag, _value, _private, _category );
				else
					local _links = self:GetLinks( _id );
					if ( table.Count( _links ) > 0 ) then
						debug.print( "Data:SetPrivateLinkedFlag", "Updated Flag, Entity:", _ent, "Flag:", _flag, "Value:", _value, "Private:", _private );
						networking:SendToClient( "EntitySetFlag", _linkedlist, _id, _flag, _value, _private, _category );
					end
				end
			else
				// Globally known flag, Tell the world
				debug.print( "Data:SetPublicFlag", "Updated Flag, Entity:", _ent, "Flag:", _flag, "Value:", _value, "Private:", _private );
				networking:Broadcast( "EntitySetFlag", _id, _flag, _value, false, _category );
			end
		end
	end
end


//
// Simple helper-function to recursively chew through data and clear it from memory...
//
local function ResetTableFlags( _tab )
	if ( istable( _tab ) ) then
		for k, v in pairs( _tab ) do
			if ( istable( v ) ) then
				ResetTableFlags( v );
			else
				v = nil;
			end
		end
	end

	_tab = nil;
end


//
// Resets all flags and links associated with an entity
//
function data:ResetFlags( _id ) // , _private
	//
	local _flags = self:GetFlags( false );
	local _entflags = _flags[ _id ];
	local _pflags = self:GetFlags( true );
	local _entpflags = _pflags[ _id ];

	if ( SERVER ) then
		local _links = self:GetLinks( _id );
		if ( _links ) then
			_links = nil;
		end
	end

	if ( _entflags || _entpflags ) then
		ResetTableFlags( _entflags );
		ResetTableFlags( _entpflags );
		_flags[ _id ] = nil;
		_pflags[ _id ] = nil;

		if ( SERVER ) then
			debug.print( "Data:ResetFlags", "Networking EntityResetFlags, ID:", _id );
			networking:Broadcast( "EntityResetFlags", _id, _private );
		end

		debug.print( "Data:ResetFlags", "Reset Public / Private Flags, ID:", _id );
		return true;
	end

	debug.print( "Data:ResetFlagsError", "No Flags to Reset, ID:", _id );
	return false;
end


//
// Checks to see if _ent is linked to _p
//
function data:IsLinked( _p, _ent )
	local _pid = self:GetEntityID( _p );
	local _entid = self:GetEntityID( _ent );

	if ( !self.__flags.links[ _entid ] ) then self.__flags.links[ _entid ] = { }; end
	local _links = self.__flags.links[ self:GetEntityID( _ent ) ];
	local _linked = _links[ self:GetEntityID( _p ) ];

	return _linked == _p;
end


//
// INTERNAL: Networks the link update to client / server...
//
function data:__UpdateLink( _p, _ent, _active )
	debug.print( "Data:UpdateLink", "Player:", _p, "Entity:", _ent, "Active:", _active );

	if ( CLIENT ) then
		networking:SendToServer( "UpdateDataLink", _ent, _active );
	else
		networking:SendToClient( "UpdateDataLink", _p, _ent, _active );
	end
end


//
// INTERNAL: Handles linking / unlinking of data
//
function data:__LinkData( _p, _ent, _active )
	// Grab ids
	local _pid = self:GetEntityID( _p );
	local _entid = self:GetEntityID( _ent );

	// Ensure initialized
	if ( !self.__flags.links[ _entid ] ) then self.__flags.links[ _entid ] = { }; end

	// Link the data...
	if ( !self.__flags.links[ _entid ][ _pid ] && _active ) then
		self.__flags.links[ _entid ][ _pid ] = _p;
		data:__UpdateLink( _p, _ent, _active );
	elseif ( self.__flags.links[ _entid ][ _pid ] && !_active ) then
		self.__flags.links[ _entid ][ _pid ] = nil;
		data:__UpdateLink( _p, _ent, _active );
	end
end


//
// Add a link so that private flags for _ent will also be sent to _p
//
function data:LinkData( _p, _ent )
	self:__LinkData( _p, _ent, true );

	if ( SERVER ) then
		debug.print( "Data:LinkData", "Player:", _p, "Entity:", _ent );
		local _entid = self:GetEntityID( _ent );
		networking:SendToClient( "SyncPData", _p, _entid, self.__flags.private[ _entid ] );
	end
end


//
// Terminates private data link
//
function data:UnlinkData( _p, _ent )
	self:__LinkData( _p, _ent, false );

	if ( CLIENT ) then
		debug.print( "Data:UnLinkData", "Player:", _p, "Entity:", _ent );
		local _entid = self:GetEntityID( _ent );
		self.__flags.private[ _entid ] = nil;
	end
end


//
// Networking
//
-- hook.Add( "PostInitNetworking", "Data:AddNetReceivers", function( ) -- Don't need this with require_once
	MsgC( COLOR_RED, "Initializing Data Networking Receivers!\n" );

	if ( CLIENT ) then
		//
		// Updates a flag
		//
		networking:AddReceiver( "EntitySetFlag", function( _lp, _ent, _flag, _value, _private, _category )
			debug.print( "Data:RcvEntitySetFlag", "Entity:", _ent, "Flag:", _flag, "Value:", _value, "Private:", _private, "Category:", _category );
			data:SetFlag( _ent, _flag, _value, _private, _category );
		end );


		//
		// Processes resetting of all flags...
		//
		networking:AddReceiver( "EntityResetFlags", function( _lp, _id )
			debug.print( "Data:RcvEntityResetFlags", "ID:", _id );
			local _ent = Entity( _id );
			data:ResetFlags( _id );
		end );


		//
		// Network Receiver which grabs all private data and sets it on the client
		//
		networking:AddReceiver( "SyncPData", function( _lp, _id, _tab )
			debug.print( "Data:RcvSyncPData", "ID:", _id );
			data:SetPData( _id, _tab )
		end );


		//
		// Network Receiver which grabs all public data and sets it on the client
		//
		networking:AddReceiver( "SyncData", function( _lp, _tab )
			debug.print( "Data:RcvSyncData", "ID:", _id );
			return data:SetFlags( _tab );
		end );
	end


	//
	// Processes the Linking and Unlinking of data
	//
	networking:AddReceiver( "UpdateDataLink", function( _p, _ent, _active )
		debug.print( "Data:RcvUpdateDataLink", "Player:", _p, "Entity:", _ent, "Active:", _active );
		if ( !_active ) then
			data:UnlinkData( _p, _ent );
		else
			data:LinkData( _p, _ent );
		end
	end );
-- end );