//
// LRecipentFilter Meta-Table / Class - Josh 'Acecool' Moser
//


// Set up a new table, which will become a metatable
LRecipientFilter = { };

// Set up a default table for recipients so it'll be copied over
LRecipientFilter.__Recipients = { };

// What gets requested when LRecipientFilter is accessed
LRecipientFilter.__index = LRecipientFilter;

// For advanced Typing
LRecipientFilter.__type = "LRecipientFilter";

// When you call the LRecipientFilter as a function, it outputs the players as a table!
// _filter = RecipientFilter( ); _filter:AddAllPlayers( ); print( _filter( ) ); -- Prints a table of all players on server!
LRecipientFilter.__call = function( self ) return self:GetRecipients( ); end
LRecipientFilter.__tostring = function( )
	return "LRecipientFilter";
end


//
// Returns the list of players in the filter in table format ( what net-messages require )
//
function LRecipientFilter:GetRecipients( )
	return self.__Recipients;
end



//
// Add all players to the filter
//
function LRecipientFilter:AddAllPlayers( )
	self.__Recipients = player.GetAll( );
end


//
// Remove all players from the filter ( resets the table )
//
function LRecipientFilter:RemoveAllPlayers( )
	self.__Recipients = { };
end


//
// Internal function which we use to Add/Remove Players in PVS via Callback ( simplifies things / reduces repeated code )
//
function LRecipientFilter:__PVSCheck( _pos, _callback )
	// DUPLICATE CODE ( Will be here until the player spawn system is implemented which isues this... )
	// Decided to make this a global to avoid running ents.FindByClass each time...
	if ( !MOBILE_SPAWN_POINT || !IsValid( MOBILE_SPAWN_POINT ) ) then
		// Find a mobile spawn...
		local _mobileSpawn = ents.FindByClass( 'info_mobile_spawn' )[ 1 ];

		// If there is no mobile spawn-point, create it...
		if ( !IsValid( _mobileSpawn ) ) then
			_mobileSpawn = ents.Create( "info_mobile_spawn" );
			if ( !_mobileSpawn ) then return false; end

			_mobileSpawn:SetPos( Vector( 0, 0, 0 ) );
			_mobileSpawn:SetColor( Color( 0,0,0, 0 ) );
			_mobileSpawn:Spawn( );
		end

		// Set it...
		MOBILE_SPAWN_POINT = _mobileSpawn;
	end

	// If this isn't valid, there are no players... ( DUPLICATE CODE REPLACEMENT; this is what will be used when the above code is removed )
	if ( !MOBILE_SPAWN_POINT ) then return false; end

	// Hijack out spawn "point" entity to use for PVS verification...
	MOBILE_SPAWN_POINT:SetPos( _pos );

	for k, _p in pairs( player.GetAll( ) ) do
		if ( !IsValid( _p ) ) then continue; end
		// Only works if the mobile spawn point checks visible...
		local _pvs = MOBILE_SPAWN_POINT:Visible( _p );
		if ( _pvs ) then
			_callback( _p, _pvs )
		end
	end
end


//
// Add all Players in PVS to the filter
//
function LRecipientFilter:AddPVS( _pos, _remove )
	// Allow Player to be used as "position"
	if ( _pos && _pos:IsPlayer( ) ) then _pos = _pos:GetPos( ); end

	self:__PVSCheck( _pos, function( _p, _pvs )
		self:AddPlayer( _p );
	end );
end


//
// Remove all Players in PVS from the filter
//
function LRecipientFilter:RemovePVS( _pos )
	// Allow Player to be used as "position"
	if ( _pos && _pos:IsPlayer( ) ) then _pos = _pos:GetPos( ); end

	self:__PVSCheck( _pos, function( _p, _pvs )
		self:RemovePlayer( _p );
	end );
end


//
// Add a Player to the filter
//
function LRecipientFilter:AddPlayer( _p )
	if ( !IsValid( _p ) ) then error( "Invalid Player", _p ); end
	if ( !table.HasValue( self.__Recipients, _p ) ) then
		table.insert( self.__Recipients, _p );
	end
end


//
// Remove a Player from the filter
//
function LRecipientFilter:RemovePlayer( _p )
	if ( !IsValid( _p ) ) then error( "Invalid Player", _p ); end
	local _key = table.KeyFromValue( self.__Recipients, _p );
	if ( _key ) then
		table.remove( self.__Recipients, _key );
	end
end


//
// Create a new LRecipientFilter
//
function LRecipientFilter:New( )
	local __class = {
		__Recipients = { };
	};
	setmetatable( __class, self );
	return __class;
end


//
// Create a new LRecipientFilter
//
function RecipientFilter( )
	return LRecipientFilter:New( );
end


//
// Debugging
//
function LRecipientFilter:GenerateExample( )
	local _p = Entity( 1 );
	-- local _p2 = Entity( 2 );

	local _rf1 = RecipientFilter( );
	local _rf2 = RecipientFilter( );
	local _rf3 = RecipientFilter( );

	print( 1, _rf1 );
	print( 2, _rf2 );
	print( 3, _rf3 );

	print( "Adding Player:", _p, "\n" );
	_rf1:AddPlayer( _p );

	print( "Adding PVS from Player Position:", _p, _p:GetPos( ), "\n" );
	_rf2:AddPVS( _p );

	print( 1, _rf1 );
	print( 2, _rf2 );
	print( 3, _rf3 );
end