//
//  - Josh 'Acecool' Moser
//

if ( xgui != nil ) then return; end 	// Don't bother the server with a hook if they don't even have ULX or the file causing the issue

hook.Add( "Initialize", "VerifyULXVersion", function( )
	//
	// If they are using ULX, check the version to see if it has the non-should've_been-local-variable problem...
	//
	local _, _ver = ulx.getVersion()	// ULX's "excellent" versioning system
	local _ver = tonumber( _ver ) != nil and tonumber( _ver ) or 0
	if ( _ver == 0 ) then
		MsgC( Color( 255, 0, 0, 255 ), "[ AcecoolDev_Networking ] NOTICE:", color_white, "Could not verify ULX version! Make sure you have not deleted the version file\n\n" );
	elseif ( _ver <= 3.61 ) then
		MsgC( Color( 255, 0, 0, 255 ), "[ AcecoolDev_Networking ] NOTICE:", color_white, " You're running ULX version 3.61 or prior, these versions of ULX has a flaw which prevents AcecoolDev_Networking from working properly... Current Version: " .. _version .. "\n\n" );
		MsgC( color_white, "To fix the issue, modify addons/ulx/lua/ulx/modules/xgui_server.lua LINE 152\n\n" );
		MsgC( color_white, "CHANGE\n\n\t\tdata = xgui.dataTypes[dtype]\n\n" );
		MsgC( color_white, "TO\n\n\t\tlocal data = xgui.dataTypes[dtype]\n\n" );
		MsgC( Color( 255, 0, 0, 255 ), "[ AcecoolDev_Networking ] NOTICE:", color_white, " i.e. simply add \"local \" to the data variable so that it becomes a local variable instead of a global variable...\n\nFeel free to remove: addons/acecooldev_networking/lua/acecooldev_networking/addons/sv_check_ulx_version.lua once the fix has been applied if you're tired of seeing this notice!\n\n" );
	end
end );