//
// Converts UMsg, NWVars, etc... to use my networking system - Josh 'Acecool' Moser
//
if ( SERVER ) then
	//
	// We do need to store some data from an active user-message, namely the recipients...
	// We'll only use one net-message to transfer all these umsgs through...
	//
	PENDING_USERMESSAGE = { };
	util.AddNetworkString( "LUMessage" );


	//
	// Start just uses net.Start, plus a net.WriteString for the name of the message...
	//
	function umsg.Start( _name, _players )
		// Make sure we're not overlapping...
		if ( PENDING_USERMESSAGE.active ) then
			ErrorNoHalt( "You must dispatch the current UMSG using umsg.End( ) before Starting another!" );
			print( "PENDING_USERMESSAGE DATA: " .. PENDING_USERMESSAGE.name );
			PENDING_USERMESSAGE = { };
			return;
			-- umsg.End( );
		end

		// Set the pending as active so we don't overlap
		PENDING_USERMESSAGE.name = _name;
		PENDING_USERMESSAGE.active = true;
		PENDING_USERMESSAGE.recipients = _players;

		// Write the starting data...
		net.Start( "LUMessage" );
			net.WriteString( _name );
	end


	//
	// Work-horse functions
	//
	function umsg.Angle( _ang ) net.WriteAngle( _ang || angle_zero ); end
	function umsg.Bool( _bit ) net.WriteBit( _bit || 0 ); end
	function umsg.Char( _char ) net.WriteInt( ( isstring( _char ) && string.char( _char ) || _char ), 8 ); end
	function umsg.Entity( _ent )
		local _valid = IsValid( _ent );
		local _id = -1;
		if ( _valid ) then
			_id = _ent:EntIndex( );
		end
		net.WriteInt( _id, 16 );
	end
	function umsg.Float( _float ) net.WriteFloat( _float || 0.0 ); end
	function umsg.Long( _long ) net.WriteInt( _long || 0, 32 ); end
	function umsg.Short( _short ) net.WriteInt( _short || 0, 16 ); end
	function umsg.String( _string ) net.WriteString( _string || "" ); end
	function umsg.Vector( _vector ) net.WriteVector( _vector || vector_origin ); end
	function umsg.VectorNormal( _vector )
		if ( type( _vector ) == "Vector" ) then 
			_vector:Normalize(); 
		end 
		net.WriteVector( _vector || vector_origin ); 
	end


	//
	// End is what sends it. Somewhat annoying levels of code to sort out recipient-filter from table from user from nothing.
	//
	function umsg.End( )
		// Convert _players from RecipientFilter to table...
		local _players = PENDING_USERMESSAGE.recipients;
		if ( istable( _players ) || type( _players ) == "LRecipientFilter" ) then
			if ( _players.GetRecipients ) then
				PENDING_USERMESSAGE.recipients = _players:GetRecipients( );
			else
				PENDING_USERMESSAGE.recipients = _players;
			end
		else
			if ( IsValid( _players ) ) then
				PENDING_USERMESSAGE.recipients = _players;
			end
		end

		// Broadcast or send...
		if ( !PENDING_USERMESSAGE.recipients ) then
			net.Broadcast( );
			print( "UMSG Broadcasting: " .. PENDING_USERMESSAGE.name );
		else
			net.Send( PENDING_USERMESSAGE.recipients );
			print( "UMSG Sending: " .. PENDING_USERMESSAGE.name );
		end

		// Reset the pending message
		PENDING_USERMESSAGE = { };
	end
else
	//
	// The work-horse reader functions and conversions...
	//
	function usermessage:ReadAngle( ) return net.ReadAngle( ); end
	function usermessage:ReadBool( ) return tobool( net.ReadBit( ) ); end
	function usermessage:ReadChar( ) return net.ReadInt( 8 ); end
	function usermessage:ReadEntity( )
		local _id = net.ReadInt( 16 );
		if ( _id == -1 ) then
			return NULL;
		else
			return Entity( _id ) || NULL;
		end
	end
	function usermessage:ReadFloat( ) return net.ReadFloat( ); end
	function usermessage:ReadLong( ) return net.ReadInt( 32 ); end
	function usermessage:ReadShort( ) return net.ReadInt( 16 ); end
	function usermessage:ReadString( ) return net.ReadString( ); end
	function usermessage:ReadVector( ) return net.ReadVector( ); end
	function usermessage:ReadVectorNormal( ) return net.ReadVector( ); end
	function usermessage:Reset( ) ErrorNoHalt( "usermessage.Reset( ) is the one function I didn't convert because it would require too much overhead.. Store the reads into vars and go back to them that way!" ); end


	//
	// One Net Message to receive them all
	//
	net.Receive( "LUmessage", function( _len, _p )
		// We need the name
		local _name = net.ReadString( _name );

		// To get the callback
		local _callback = usermessage.__hooks[ _name ];

		// To call the callback
		if ( _callback ) then
			// And we'll pass the usermessage function library through, because normally it is the "msg/data"
			// var which references the actual data. I didn't set up an entire table and just did a quick and
			// dirty conversion so it just maps the functions essentially. Reset won't work, but with that being
			// the only downside, I can live with it.
			_callback( usermessage );
		else
			// Error if something wrong happened...
			error( "Undefined UMSG Hook: " .. _name );
		end
	end );

	//
	// This registers the user-message hook into the function library
	//
	usermessage.__hooks = usermessage.__hooks || { };
	function usermessage.Hook( _name, _callback )
		usermessage.__hooks[ _name ] = _callback;
	end

	// Since GetTable isn't redirected, we can scan it so if umsg.Hook wasn't included quickly enough, it'll still
	// process all pre-existing functions, then remove them from the table so that it will be auto-refresh compatible.
	for _name, v in pairs( usermessage:GetTable( ) ) do
		usermessage.Hook( _name, v.Function );
		usermessage:GetTable( )[ _name ] = nil;
	end

	//
	// No idea
	//
	function usermessage.IncomingMessage( _name, _msg )
		print( "UMSG: ", _name, _msg );
	end
end