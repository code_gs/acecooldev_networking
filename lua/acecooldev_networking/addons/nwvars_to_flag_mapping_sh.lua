//
// Convers a lot of the NW vars into Flags ( meaning server won't update client unless var changes ) - Josh 'Acecool' Moser
//


//
// Init / Vars
//


//
// These are defaults to certain flags just in case Set* is called without valid data...
//
local DEFAULT_VALUE_MAPPING = {
	// _name Overrides
	TopColor			= Vector( 0.2, 0.5, 1 );
	BottomColor			= Vector( 0.8, 1, 1 );
	SunNormal			= Vector( -0.24, 0.44, 0.87 );
	SunColor			= Vector( 0.2, 0.1, 0 );
	DuskColor			= Vector( 1, 0.2, 0 );
	FadeBias			= 1;
	HDRScale			= 0.66;
	DuskScale			= 1;
	DuskIntensity		= 1;
	SunSize				= 2;
	StarScale			= 0.5;
	StarFade			= 0.5;
	StarSpeed			= 0.01;
	StarTexture			= "skybox/clouds";

	// _type fields
	Angle				= Angle( 0, 0, 0 ); -- Not using angle_zero because of pass by reference...
	Bool				= false;
	Entity				= Entity( 0 );
	Float				= 0;
	Int					= 0;
	String				= "";
	Vector				= Vector( 0, 0, 0 ); -- Not using vector_* because of pass by reference...

	// TYPE_ ENUMS
	[ TYPE_ANGLE ]		= Angle( 0, 0, 0 ); -- Not using angle_zero because of pass by reference...
	[ TYPE_BOOL ]		= false;
	[ TYPE_ENTITY ]		= Entity( 0 ); -- Use world entity instead of empty entity for flag system...
	[ TYPE_NUMBER ]		= 0;
	[ TYPE_STRING ]		= "";
	[ TYPE_VECTOR ]		= Vector( 0, 0, 0 ); -- Not using vector_* because of pass by reference...

	// Unknown Types
	proxy				= nil;
	var					= "undefined_var";

	// Possibly used
	[ TYPE_FUNCTION ]	= function( ) end;
	[ TYPE_NIL ]		= nil;
	[ TYPE_TABLE ]		= { }; -- table
};


//
// Flags
//
local FLAG_NWVARS		= "nwvars";
local FLAG_GLOBALVARS	= "globalvars";
local FLAG_DTVARS		= "dtvars";
local FLAG_TABLEVARS	= "tablevars";


//
// Functions
//


//
// Base NW Setter
//
local function NWSetter( self, _key, _value )
	self:SetFlag( _key, _value, false, FLAG_NWVARS );
end


//
// Base NW Getter
//
local function NWGetter( self, _key, _default )
	return self:GetFlag( _key, _default, false, FLAG_NWVARS );
end


//
// NW Setters
//
META_ENTITY.SetNWAngle			= NWSetter;
META_ENTITY.SetNWBool			= NWSetter;
META_ENTITY.SetNWEntity			= NWSetter;
META_ENTITY.SetNWFloat			= NWSetter;
META_ENTITY.SetNWInt			= NWSetter;
META_ENTITY.SetNWString			= NWSetter;
META_ENTITY.SetNWVarProxy		= NWSetter;
META_ENTITY.SetNWVector			= NWSetter;

META_ENTITY.SetNetworkedAngle	= NWSetter;
META_ENTITY.SetNetworkedBool	= NWSetter;
META_ENTITY.SetNetworkedEntity	= NWSetter;
META_ENTITY.SetNetworkedFloat	= NWSetter;
META_ENTITY.SetNetworkedInt		= NWSetter;
META_ENTITY.SetNetworkedString	= NWSetter;
META_ENTITY.SetNetworkedVar		= NWSetter;
META_ENTITY.SetNetworkedVector	= NWSetter;


//
// NW Getters
//
function META_ENTITY:GetNWAngle( _key, _default )
	return NWGetter( self, _key, _default || DEFAULT_VALUE_MAPPING[ TYPE_ANGLE ] );
end

function META_ENTITY:GetNWBool( _key, _default )
	return NWGetter( self, _key, _default || DEFAULT_VALUE_MAPPING[ TYPE_BOOL ] );
end

function META_ENTITY:GetNWEntity( _key, _default )
	return NWGetter( self, _key, _default || DEFAULT_VALUE_MAPPING[ TYPE_ENTITY ] );
end

function META_ENTITY:GetNWFloat( _key, _default )
	return NWGetter( self, _key, _default || DEFAULT_VALUE_MAPPING[ TYPE_NUMBER ] );
end

function META_ENTITY:GetNWInt( _key, _default )
	return NWGetter( self, _key, _default || DEFAULT_VALUE_MAPPING[ TYPE_NUMBER ] );
end

function META_ENTITY:GetNWString( _key, _default )
	return NWGetter( self, _key, _default || DEFAULT_VALUE_MAPPING[ TYPE_STRING ] );
end

function META_ENTITY:GetNWVarProxy( _key, _default )
	return NWGetter( self, _key, _default || DEFAULT_VALUE_MAPPING.proxy );
end

function META_ENTITY:GetNWVar( _key, _default )
	return NWGetter( self, _key, _default || DEFAULT_VALUE_MAPPING[ TYPE_STRING ] );
end

function META_ENTITY:GetNWVector( _key, _default )
	return NWGetter( self, _key, _default || DEFAULT_VALUE_MAPPING[ TYPE_VECTOR ] );
end


//
// NW Getter Aliases
//
META_ENTITY.GetNetworkedAngle	= META_ENTITY.GetNWAngle;
META_ENTITY.GetNetworkedBool	= META_ENTITY.GetNWBool;
META_ENTITY.GetNetworkedEntity	= META_ENTITY.GetNWEntity;
META_ENTITY.GetNetworkedFloat	= META_ENTITY.GetNWFloat;
META_ENTITY.GetNetworkedInt		= META_ENTITY.GetNWInt;
META_ENTITY.GetNetworkedString	= META_ENTITY.GetNWString;
META_ENTITY.GetNetworkedVar		= META_ENTITY.GetNWVar;
META_ENTITY.GetNetworkedVector	= META_ENTITY.GetNWVector;


//
// Important Function which creates Getters / Setters...
//
function META_ENTITY:NetworkVar( _type, _slot, _name, _extended )
	// Key is either the extended key-name ( unique ) or Type + Slot, ie Vector0, Vector1, Vector2, etc...
	local _key = _extended && _extended.KeyName || ( _type .. tostring( _slot ) );

	// Reversed so that specific names can have other default values if they exist.
	local _unset_default = DEFAULT_VALUE_MAPPING[ _name ] || DEFAULT_VALUE_MAPPING[ _type ];

	// Setter Function
	self[ "Set" .. _name ] = function( self, _value )
		NWSetter( self, _key, _value || _unset_default );
	end

	// Getter Function
	self[ "Get" .. _name ] = function( self, _default )
		return NWGetter( self, _key, _default || _unset_default );
	end
end


//
// Base Global Setter
//
local function GlobalSetter( _key, _value )
	data:SetFlag( 0, _key, _value, false, FLAG_GLOBALVARS );
end


//
// Base Global Setter
//
local function GlobalGetter( _key, _default )
	return data:GetFlag( 0, _key, _default, false, FLAG_GLOBALVARS );
end


//
// Global Setters
//
SetGlobalAngle	= GlobalSetter;
SetGlobalBool	= GlobalSetter;
SetGlobalEntity	= GlobalSetter;
SetGlobalFloat	= GlobalSetter;
SetGlobalInt	= GlobalSetter;
SetGlobalString	= GlobalSetter;
SetGlobalVector	= GlobalSetter;

// Default behavior isn't defined in Wiki if called on Client... for security I'll leave the change on
// the client and NOT network it just as the flag system does for flags.
SetGlobalVar	= GlobalSetter;


//
// Global Getters
//
function GetGlobalAngle( _key, _default )
	return GlobalGetter( _key, _default || DEFAULT_VALUE_MAPPING[ TYPE_ANGLE ] );
end

function GetGlobalBool( _key, _default )
	return GlobalGetter( _key, _default || DEFAULT_VALUE_MAPPING[ TYPE_BOOL ] );
end

function GetGlobalEntity( _key, _default )
	return GlobalGetter( _key, _default || DEFAULT_VALUE_MAPPING[ TYPE_ENTITY ] );
end

function GetGlobalFloat( _key, _default )
	return GlobalGetter( _key, _default || DEFAULT_VALUE_MAPPING[ TYPE_NUMBER ] );
end

function GetGlobalInt( _key, _default )
	return GlobalGetter( _key, _default || DEFAULT_VALUE_MAPPING[ TYPE_NUMBER ] );
end

function GetGlobalString( _key, _default )
	return GlobalGetter( _key, _default || DEFAULT_VALUE_MAPPING[ TYPE_STRING ] );
end

function GetGlobalVector( _key, _default )
	return GlobalGetter( _key, _default || DEFAULT_VALUE_MAPPING[ TYPE_VECTOR ] );
end

function GetGlobalVar( _key, _default )
	return GlobalGetter( _key, _default || DEFAULT_VALUE_MAPPING.var );
end


//
// Including the following jumble of mess because I am working on converting these too but they'll require a different strategy!
//
-- local function DTSetter( self, _key, _value ) self:SetFlag( _key, _value, false, FLAG_DTVARS ); end
-- local function DTGetter( self, _key, _default ) return self:GetFlag( _key, _default, false, FLAG_DTVARS ); end

-- META_ENTITY.SetDTAngle = DTSetter;
-- META_ENTITY.GetDTAngle = DTGetter;

-- META_ENTITY.SetDTBool = DTSetter;
-- META_ENTITY.GetDTBool = DTGetter;

-- META_ENTITY.SetDTEntity = DTSetter;
-- META_ENTITY.GetDTEntity = DTGetter;

-- META_ENTITY.SetDTFloat = DTSetter;
-- META_ENTITY.GetDTFloat = DTGetter;

-- META_ENTITY.SetDTInt = DTSetter;
-- META_ENTITY.GetDTInt = DTGetter;

-- META_ENTITY.SetDTString = DTSetter;
-- META_ENTITY.GetDTString = DTGetter;

-- META_ENTITY.SetDTVector = DTSetter;
-- META_ENTITY.GetDTVector = DTGetter;

-- local FLAG_SAVEVARS = "savevars";
-- function META_ENTITY:SetSaveValue( _flag, _value )
	-- self:SetFlag( _flag, _value, false, FLAG_SAVEVARS );
-- end;

-- function META_ENTITY:GetSaveTable( )
	-- local _tab = data:GetFlags( )[ 0 ];
	-- local _lists = _tab.lists;
	-- if ( _lists ) then
		-- return _lists[ FLAG_SAVEVARS ];
	-- else
		-- return _tab.default;
	-- end
-- end

-- function META_ENTITY:GetInternalVariable( _flag, _default )
	-- return self:GetFlag( _flag, _default, false, FLAG_SAVEVARS );
-- end


//
//
//
-- function META_ENTITY:SetVar( _flag, _value )
	-- self:SetFlag( _flag, _value, false, FLAG_TABLEVARS );
-- end

-- function META_ENTITY:GetVar( _flag, _default )
	-- return self:GetFlag( _flag, _default, false, FLAG_TABLEVARS );
-- end


--[[
Entity:SetSaveValue( string name, any value )
Entity:GetSaveTable( )
Entity:SetTable( table tab )
]]

-- local _p = Entity( 1 );
-- print( _p:GetSaveTable( ) );
-- print( _p:GetInternalVariable( "velocity" ) );
-- print( _p:GetTable( ) );
-- print( _p:GetVar( "InSwim" ) );