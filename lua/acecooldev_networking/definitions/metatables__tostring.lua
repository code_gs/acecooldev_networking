//
// Meta-Table Loader - Josh 'Acecool' Moser
//


//**
// Variables
//**
local DEPTH_DELIMITER = "\t";


//**
// COMMON MetaTables
//**

//
// Entity > Player tostring
//
function META_PLAYER:__tostring( )
	return "Player [" .. tostring( self:GetID( ) ) .. "][" .. self:Nick( ) .. "][" .. self:SteamID( ) .. "] ";
end


//
// Entity tostring
//
function META_ENTITY:__tostring( )
	if ( IsValid( self ) ) then
		return "Entity [" .. tostring( self:GetID( ) ) .. "][" .. self:GetClass( ) .. "] ";
	elseif ( self:EntIndex( ) == 0 ) then
		return "Entity [0][worldspawn]";
	else
		return "[NULL ENTITY]";
	end
end


//
// Entity > Weapon tostring
//
function META_WEAPON:__tostring( )
	return "Weapon [" .. tostring( self:GetID( ) ) .. "][" .. self:GetClass( ) .. "] ";
end


//
// Entity > Vehicle tostring
//
function META_VEHICLE:__tostring( )
	return "Vehicle [" .. tostring( self:GetID( ) ) .. "][" .. self:GetClass( ) .. "] ";
end

//
// Color
//
function META_COLOR:__tostring( )
	return "Color( " .. self.r || 255 .. ", " .. self.g || 255 .. ", " .. self.b || 255 .. ", " .. self.a || 255 .. " );";
end


//
// ConVar tostring
//
function META_CONVAR:__tostring( )
	return "META_CONVAR";
end


//**
// NOT SO COMMON
//**

//
// Entity > NPC tostring
//
function META_NPC:__tostring( )
	return "NPC [" .. tostring( self:GetID( ) ) .. "][" .. self:GetClass( ) .. "] ";
end


//
// Material tostring
//
function META_IMATERIAL:__tostring( )
	-- local _depth = string.rep( DEPTH_DELIMITER, 1 + depth );
	local _depth = "\t";
	return "MATERIAL:\n" .. _depth .. "GetColor( x = 0, y = 0 )\t" .. toprint( self:GetColor( 0, 0 ) ) ..
		-- "\nGetFloat\t\t\t" .. toprint( self:GetFloat( tostring( v ) ) ) ..
		-- "\nGetInt\t\t\t\t" .. toprint( self:GetInt( tostring( v ) ) ) ..
		-- "\nGetMatrix\t\t\t" .. toprint( self:GetMatrix( tostring( v ) ) ) ..
		"\n" .. _depth .. "GetName\t\t\t\t" .. toprint( self:GetName( tostring( v ) ) ) ..
		"\n" .. _depth .. "GetShader\t\t\t" .. toprint( self:GetShader( tostring( v ) ) ) ..
		-- "\nGetString\t\t\t" .. toprint( self:GetString( tostring( v ) ) ) ..
		-- "\nGetTexture\t\t\t" .. toprint( self:GetTexture( tostring( v ) ) ) ..
		-- "\nGetVector\t\t\t" .. toprint( self:GetVector( tostring( v ) ) ) ..
		"\n" .. _depth .. "Height\t\t\t\t" .. toprint( self:Height( ) ) ..
		"\n" .. _depth .. "Width\t\t\t\t" .. toprint( self:Width( ) ) ..
		"\n" .. _depth .. "IsError\t\t\t\t" .. toprint( self:IsError( ) );
end


//
// Restore tostring
//
function META_IRESTORE:__tostring( )
	return "META_IRESTORE";
end


//
// table > Vector tostring
//
function META_VECTOR:__tostring( )
	return "Vector( " .. self.x .. ", " .. self.y .. ", " .. self.z .. " )";
end


//
// Save tostring
//
function META_ISAVE:__tostring( )
	return "META_ISAVE";
end


//
// UserCMD tostring
//
function META_CUSERCMD:__tostring( )
	local _depth = "\t";
	return "USER COMMAND: \n" .. _depth .. "CommandNumber\t\t" .. toprint( self:CommandNumber( ) ) ..
		"\n" .. _depth .. "GetButtons\t\t" .. toprint( self:GetButtons( ) ) ..
		"\n" .. _depth .. "GetForwardMove\t\t" .. toprint( self:GetForwardMove( ) ) ..
		"\n" .. _depth .. "GetImpulse\t\t" .. toprint( self:GetImpulse( ) ) ..
		"\n" .. _depth .. "GetMouseWheel\t\t" .. toprint( self:GetMouseWheel( ) ) ..
		"\n" .. _depth .. "GetMouseX\t\t" .. toprint( self:GetMouseX( ) ) ..
		"\n" .. _depth .. "GetMouseY\t\t" .. toprint( self:GetMouseY( ) ) ..
		"\n" .. _depth .. "GetSideMove\t\t" .. toprint( self:GetSideMove( ) ) ..
		"\n" .. _depth .. "GetUpMove\t\t" .. toprint( self:GetUpMove( ) ) ..
		"\n" .. _depth .. "GetViewAngles\t\t" .. toprint( self:GetViewAngles( ) ) ..
		"\n" .. _depth .. "TickCount\t\t" .. toprint( self:TickCount( ) ), PRINT_COLORS.UserCmd;
end


//
// Sound tostring
//
function META_CSOUNDPATCH:__tostring( )
	return "SOUND PATCH: \nIsPlaying\t\t" .. toprint( self:IsPlaying( ) );
end


//
// MoveData tostring
//
function META_CMOVEDATA:__tostring( )
	local _depth = "\t";
	return "MOVE DATA:\n" .. _depth .. "GetAbsMoveAngles\t" .. toprint( self:GetAbsMoveAngles( ) ) ..
		"\n" .. _depth .. "GetAngles\t\t" .. toprint( self:GetAngles( ) ) ..
		"\n" .. _depth .. "GetButtons\t\t" .. toprint( self:GetButtons( ) ) ..
		"\n" .. _depth .. "GetConstraintRadius\t" .. toprint( self:GetConstraintRadius( ) ) ..
		"\n" .. _depth .. "GetForwardSpeed\t\t" .. toprint( self:GetForwardSpeed( ) ) ..
		"\n" .. _depth .. "GetImpulseCommand\t" .. toprint( self:GetImpulseCommand( ) ) ..
		"\n" .. _depth .. "GetMaxClientSpeed\t" .. toprint( self:GetMaxClientSpeed( ) ) ..
		"\n" .. _depth .. "GetMaxSpeed\t\t" .. toprint( self:GetMaxSpeed( ) ) ..
		"\n" .. _depth .. "GetMoveAngles\t\t" .. toprint( self:GetMoveAngles( ) ) ..
		"\n" .. _depth .. "GetOldAngles\t\t" .. toprint( self:GetOldAngles( ) ) ..
		"\n" .. _depth .. "GetOldButtons\t\t" .. toprint( self:GetOldButtons( ) ) ..
		"\n" .. _depth .. "GetOrigin\t\t" .. toprint( self:GetOrigin( ) ) ..
		"\n" .. _depth .. "GetSideSpeed\t\t" .. toprint( self:GetSideSpeed( ) ) ..
		"\n" .. _depth .. "GetUpSpeed\t\t" .. toprint( self:GetUpSpeed( ) ) ..
		"\n" .. _depth .. "GetVelocity\t\t" .. toprint( self:GetVelocity( ) ), PRINT_COLORS.MoveData;
end


//
// EffectData tostring
//
function META_CEFFECTDATA:__tostring( )
	local _depth = "\t";
	return "EFFECT DATA:\n" .. _depth .. "GetAngles\t\t" .. toprint( self:GetAngles( ) ) ..
		"\n" .. _depth .. "GetAttachment\t\t" .. toprint( self:GetAttachment( ) ) ..
		"\n" .. _depth .. "GetColor\t\t" .. toprint( self:GetColor( ) ) ..
		"\n" .. _depth .. "GetDamageType\t\t" .. toprint( self:GetDamageType( ) ) ..
		"\n" .. _depth .. "GetEntIndex\t\t" .. toprint( ( self.GetEntIndex && self:GetEntIndex( ) || "N/A" ) ) ..
		"\n" .. _depth .. "GetEntity\t\t" .. toprint( self:GetEntity( ) ) ..
		"\n" .. _depth .. "GetFlags\t\t" .. toprint( self:GetFlags( ) ) ..
		"\n" .. _depth .. "GetHitBox\t\t" .. toprint( self:GetHitBox( ) ) ..
		"\n" .. _depth .. "GetMagnitude\t\t" .. toprint( self:GetMagnitude( ) ) ..
		"\n" .. _depth .. "GetMaterialIndex\t" .. toprint( self:GetMaterialIndex( ) ) ..
		"\n" .. _depth .. "GetNormal\t\t" .. toprint( self:GetNormal( ) ) ..
		"\n" .. _depth .. "GetOrigin\t\t" .. toprint( self:GetOrigin( ) ) ..
		"\n" .. _depth .. "GetRadius\t\t" .. toprint( self:GetRadius( ) ) ..
		"\n" .. _depth .. "GetScale\t\t" .. toprint( self:GetScale( ) ) ..
		"\n" .. _depth .. "GetStart\t\t" .. toprint( self:GetStart( ) ) ..
		"\n" .. _depth .. "GetSurfaceProp\t\t" .. toprint( self:GetSurfaceProp( ) );
end


//
// Texture tostring
//
function META_ITEXTURE:__tostring( )
	local _depth = "\t";
	return "TEXTURE:\n" .. _depth .. "GetColor( x = 0, y = 0 )\t" .. toprint( self:GetColor( 0, 0 ) ) ..
		"\n" .. _depth .. "GetMappingHeight\t\t" .. toprint( self:GetMappingHeight( ) ) ..
		"\n" .. _depth .. "GetMappingWidth\t\t\t" .. toprint( self:GetMappingWidth( ) ) ..
		"\n" .. _depth .. "GetName\t\t\t\t" .. toprint( self:GetName( ) ) ..
		"\n" .. _depth .. "Height\t\t\t\t" .. toprint( self:Height( ) ) ..
		"\n" .. _depth .. "Width\t\t\t\t" .. toprint( self:Width( ) ) ..
		"\n" .. _depth .. "IsError\t\t\t\t" .. toprint( self:IsError( ) );
end


//
// File tostring
//
function META_FILE:__tostring( )
	return "META_FILE";
end


//
// DamageInfo tostring
//
function META_CTAKEDAMAGEINFO:__tostring( )
	-- local _depth = string.rep( DEPTH_DELIMITER, 1 + depth );
	local _depth = "\t";
	return "DAMAGE INFO:\n" .. _depth .. "GetMaxDamage\t\t" .. toprint( self:GetMaxDamage( ) ) ..
		"\n" .. _depth .. "GetAttacker\t\t" .. toprint( self:GetAttacker( ) ) ..
		"\n" .. _depth .. "GetDamage\t\t" .. toprint( self:GetDamage( ) ) ..
		"\n" .. _depth .. "GetReportedPosition\t" .. toprint( self:GetReportedPosition( ) ) ..
		"\n" .. _depth .. "GetDamageForce\t\t" .. toprint( self:GetDamageForce( ) ) ..
		"\n" .. _depth .. "GetDamagePosition\t" .. toprint( self:GetDamagePosition( ) ) ..
		"\n" .. _depth .. "GetInflictor\t\t" .. toprint( self:GetInflictor( ) ) ..
		"\n" .. _depth .. "GetDamageType\t\t" .. toprint( self:GetDamageType( ) ) ..
		"\n" .. _depth .. "GetBaseDamage\t\t" .. toprint( self:GetBaseDamage( ) ) ..
		"\n" .. _depth .. "GetAmmoType\t\t" .. toprint( self:GetAmmoType( ) ) ..
		"\n" .. _depth .. "IsExplosionDamage\t" .. toprint( self:IsExplosionDamage( ) ) ..
		"\n" .. _depth .. "IsFallDamage\t\t" .. toprint( self:IsFallDamage( ) ) ..
		"\n" .. _depth .. "IsBulletDamage\t\t" .. toprint( self:IsBulletDamage( ) );
end


//
// Matrix tostring
//
function META_VMATRIX:__tostring( )
	-- local _depth = string.rep( DEPTH_DELIMITER, 1 + depth );
	local _depth = "\t";
	local _output = "VMatrix:\n" .. _depth .. "GetAngles( )\t" .. toprint( self:GetAngles( ) ) ..
		"\n" .. _depth .. "GetScale\t" .. toprint( self:GetScale( ) ) ..
		"\n" .. _depth .. "GetTranslation\t" .. toprint( self:GetTranslation( ) );

	return _output;
end


//
// table > Angle tostring
//
function META_ANGLE:__tostring( )
	return "Angle( " .. self.p .. ", " .. self.y .. ", " .. self.r .. " )";
end


//
// PhysicsObject tostring
//
function META_PHYSOBJ:__tostring( )
	return "META_PHYSOBJ";
end

if ( SERVER ) then

	//
	// LocoMotion tostring
	//
	function META_CLUALOCOMOTION:__tostring( )
		return "META_CLUALOCOMOTION";
	end


	//
	// Entity > NextBot tostring
	//
	function META_NEXTBOT:__tostring( )
		return "META_NEXTBOT";
	end


	//
	//
	//
	-- function META_DATABASE:__tostring( )
		-- local _depth = string.rep( DEPTH_DELIMITER, 1 + depth );
		-- local _depth = "\t";
		-- return "META_DATABASE";
	-- end


	//
	// PathFollower tostring
	//
	function META_PATHFOLLOWER:__tostring( )
		return "META_PATHFOLLOWER";
	end


	//
	// RecipientFilter tostring
	//
	function META_CRECIPIENTFILTER:__tostring( )
		return "Recipient Filter \n__gc\t\t"; // .. toprint( self:__gc( ) );
	end


	//
	// NavArea tostring
	//
	function META_CNAVAREA:__tostring( )
		return "META_CNAVAREA";
	end
else

	//
	// Panel tostring
	//
	function META_PANEL:__tostring( )
		if ( !ispanel( self ) || !IsValid( self ) ) then return "NULL PANEL"; end
		local _w, _h = self:GetSize( );
		local _x, _y = self:GetPos( );
		local _output = self:GetName( ) .. ": " ..
			"Class( " .. ( ( self.GetClass ) && self:GetClass( ) || "Panel" ) .. " = " .. self:GetClassName( ) .. " ); " ..
			"Pos( x = " .. _x .. "; y = " .. _y .. " ); " ..
			"Size( w = " .. _w .. "; h = " .. _h .. " );";
		-- local _output = tostring( v );

		return _output;
	end


	//
	// ParticleEmitter tostring
	//
	function META_EMITTER:__tostring( )
		return "LUA EMITTER: \nGetNumActiveParticles\t" .. toprint( self:GetNumActiveParticles( ) );
		-- "\n" .. _depth .. "Index\t\t" .. toprint( self:EntIndex( ) )
	end


	//
	// Particle tostring
	//
	function META_PARTICLE:__tostring( )
		local _depth = "\t";
		return "LUA PARTICLE:\n" .. _depth .. "GetAirResistance\t" .. toprint( self:GetAirResistance( ) ) ..
			"\n" .. _depth .. "GetAngles\t\t" .. toprint( self:GetAngles( ) ) ..
			"\n" .. _depth .. "GetAngleVelocity\t" .. toprint( self:GetAngleVelocity( ) ) ..
			"\n" .. _depth .. "GetBounce\t\t" .. toprint( self:GetBounce( ) ) ..
			"\n" .. _depth .. "GetColor\t\t" .. toprint( self:GetColor( ) ) ..
			"\n" .. _depth .. "GetDieTime\t\t" .. toprint( self:GetDieTime( ) ) ..
			"\n" .. _depth .. "GetEndAlpha\t\t" .. toprint( self:GetEndAlpha( ) ) ..
			"\n" .. _depth .. "GetEndLength\t\t" .. toprint( self:GetEndLength( ) ) ..
			"\n" .. _depth .. "GetEndSize\t\t" .. toprint( self:GetEndSize( ) ) ..
			"\n" .. _depth .. "GetGravity\t\t" .. toprint( self:GetGravity( ) ) ..
			"\n" .. _depth .. "GetLifeTime\t\t" .. toprint( self:GetLifeTime( ) ) ..
			"\n" .. _depth .. "GetPos\t\t\t" .. toprint( self:GetPos( ) ) ..
			"\n" .. _depth .. "GetRoll\t\t\t" .. toprint( self:GetRoll( ) ) ..
			"\n" .. _depth .. "GetRollDelta\t\t" .. toprint( self:GetRollDelta( ) ) ..
			"\n" .. _depth .. "GetStartAlpha\t\t" .. toprint( self:GetStartAlpha( ) ) ..
			"\n" .. _depth .. "GetStartLength\t\t" .. toprint( self:GetStartLength( ) ) ..
			"\n" .. _depth .. "GetStartSize\t\t" .. toprint( self:GetStartSize( ) ) ..
			"\n" .. _depth .. "GetVelocity\t\t" .. toprint( self:GetVelocity( ) );
	end
end

//**
// Interesting...
//**

//
// LoadLibrary tostring
//
META_LOADLIB = FindMetaTable( "_LOADLIB" );

//
// PreLoad tostring
//
META_PRELOAD = FindMetaTable( "_PRELOAD" );

//
// Loaded tostring
//
META_LOADED = FindMetaTable( "_LOADED" ); // Jit, drive, ai stuff, lots...

//
// table.ToString
//
function table.ToString( v, depth, tables_processed, lua )
	-- print( type( v.__tostring ) );
	-- if ( IsTable( v ) && v.__tostring ) then
		-- return tostring( v ), PRINT_COLORS.ToString;
	-- end
	local depth = depth || 0;
	local _out = "";
	local _depth = string.rep( DEPTH_DELIMITER, depth );
	-- //local _depth = "\t";

	// Prevents overflow...
	table.insert( tables_processed, v );

	local sorted = table.sortnonnumerickey( v, function( a, b )
		return tostring( a ) < tostring( b )
	end );

	-- for j, c in pairs( v ) do
	for i = 1, #sorted do
		local j = sorted[ i ];
		local c = v[ j ];

		if ( istable( c ) && !IsColor( c ) ) then
			// Prevent stack overflow by processing tables only 1 time.
			if ( istable( v ) && table.HasValue( tables_processed, c ) ) then
				if ( lua ) then
					_out = _out .. _depth .. "[ " .. tostring( j ) .. " ] \t= {\n" .. _depth .. tostring( c ) .. "\n" .. _depth .. "};\n";
				else
					_out = _out .. _depth .. "[" .. tostring( j ) .. "] \t= " .. tostring( c ) .. "\n";
				end
				continue;
			end
			table.insert( tables_processed, c )

			// Recursive logic.
			if ( lua ) then
				_out = _out .. _depth .. "[ " .. tostring( j ) .. " ] = {\n" .. _depth .. tostring( toprint( c, lua, depth + 1, tables_processed ) ) .. "\n" .. _depth .. "};\n";
			else
				_out = _out .. _depth .. "[" .. tostring( j ) .. "]\n"	.. tostring( toprint( c, lua, depth + 1, tables_processed ) );
			end
		else
			// Output logic.
			if ( lua ) then
				local _value = tostring( toprint( c, lua ) );
				_value = ( _value && _value != "" ) && _value || "nil";
				_out = _out .. _depth .. tostring( j ) .. _depth .. " = " .. _value .. ";\n";
			else
				_out = _out .. _depth .. "[" .. tostring( j ) .. "] \t= " .. tostring( toprint( c, lua ) ) .. "\n";
			end
		end
	end

	-- return table.ToString( v ), PRINT_COLORS.Table;
	return _out;
end


//
// Lua / GLua Meta-Tables tostring
//
function META_TABLE:__tostring( ) return table.ToString( self, 0, { }, false ); end
function META_BOOLEAN:__tostring( ) return ( self ) && "true" || "false"; end
-- function META_FUNCTION:__tostring( )
	-- local _data = debug.getinfo( _func );
	-- local _file = _data.short_src;
	-- local _source = ( _data.what == "Lua" ) && "Lua" || "C";
	-- local _tripledot = _data.isvararg;
	-- local _args = _data.nparams;
	-- local _start_line = _data.linedefined;
	-- local _last_line = _data.lastlinedefined;
	-- local _lines = _last_line - _start_line;

	-- if ( _source == "C" ) then
		-- return "C function"
	-- else
		-- local _argoutput = "";
		-- for i = 1, _args do
			-- local _out = ", ";
			-- if ( i == 1 ) then  _out = ", "; end

			-- _argoutput = _argoutput .. _out .. "_" .. i;
		-- end

		-- if ( _tripledot ) then
			-- if ( _argoutput == "" ) then
				-- _argoutput = "...";
			-- else
				-- _argoutput = _argoutput .. ", ...";
			-- end
		-- end
		-- return "Lua function( " .. _argoutput .. " ) file:[ " .. _file .. ":" .. _start_line .. "-" .. _last_line .. " ] lines: " .. _lines;
	-- end
-- end

//
// Concat functionality for the meta-tables...
//
function META_PLAYER:__concat( _text )					return string.concat( self, _text ); end
function META_ENTITY:__concat( _text )					return string.concat( self, _text ); end
function META_WEAPON:__concat( _text )					return string.concat( self, _text ); end
function META_VEHICLE:__concat( _text )					return string.concat( self, _text ); end
function META_COLOR:__concat( _text )					return string.concat( self, _text ); end
function META_CONVAR:__concat( _text )					return string.concat( self, _text ); end
function META_NPC:__concat( _text )						return string.concat( self, _text ); end
function META_IMATERIAL:__concat( _text )				return string.concat( self, _text ); end
function META_IRESTORE:__concat( _text )				return string.concat( self, _text ); end
function META_VECTOR:__concat( _text )					return string.concat( self, _text ); end
function META_ISAVE:__concat( _text )					return string.concat( self, _text ); end
function META_CUSERCMD:__concat( _text )				return string.concat( self, _text ); end
function META_CSOUNDPATCH:__concat( _text )				return string.concat( self, _text ); end
function META_CMOVEDATA:__concat( _text )				return string.concat( self, _text ); end
function META_CEFFECTDATA:__concat( _text )				return string.concat( self, _text ); end
function META_ITEXTURE:__concat( _text )				return string.concat( self, _text ); end
function META_FILE:__concat( _text )					return string.concat( self, _text ); end
function META_CTAKEDAMAGEINFO:__concat( _text )			return string.concat( self, _text ); end
function META_VMATRIX:__concat( _text )					return string.concat( self, _text ); end
function META_ANGLE:__concat( _text )					return string.concat( self, _text ); end
function META_PHYSOBJ:__concat( _text )					return string.concat( self, _text ); end

if ( SERVER ) then
	function META_CLUALOCOMOTION:__concat( _text )		return string.concat( self, _text ); end
	function META_NEXTBOT:__concat( _text )				return string.concat( self, _text ); end
	-- function META_DATABASE:__concat( _text )			return string.concat( self, _text ); end
	function META_PATHFOLLOWER:__concat( _text )		return string.concat( self, _text ); end
	function META_CRECIPIENTFILTER:__concat( _text )	return string.concat( self, _text ); end
	function META_CNAVAREA:__concat( _text )			return string.concat( self, _text ); end
else
	function META_PANEL:__concat( _text )				return string.concat( self, _text ); end
	function META_EMITTER:__concat( _text )				return string.concat( self, _text ); end
	function META_PARTICLE:__concat( _text )			return string.concat( self, _text ); end
end


//
// Lua / GLua Meta-Tables concat
//
function META_TABLE:__concat( _text )					return string.concat( table.ToString( self, 0, { }, false ), _text ); end
function META_BOOLEAN:__concat( _text )					return string.concat( self, _text ); end
-- function META_FUNCTION:__concat( _text )					return string.concat( self, _text ); end

-- print( META_FUNCTION.__concat )